@extends('layouts.app')

@section('content')
    {{--
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-comment"></span> 
                 Tareas de <i><b> {{ $titulo }} </b></i>
             </h3>
        </div> 

        <div class="panel-body">
            
            <ul class="list-group">
                @foreach($pbis as $pbi)
                    <li class="list-group-item"> 
                        <i class="fa fa-play" aria-hidden="true"></i>
                        <a href="/historias/{{ $pbi->id }}" > {{ $pbi->name }}</a>
                        <a> {{ $pbi->descripcion }} </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    
    --}}
    <div class="row task-list-row">
        <div class="col-xs-12 col-md-4">
            <ul class="list-group">
                    <h5 class="title">In Progress </h5>
                @foreach($pbis as $pbi)
                    <li>
                        <a href="/historias/{{ $pbi->id }}" > {{ $pbi->name }}</a>
                    </li>
                @endforeach
                
            </ul>
        </div>
    
        <div class="col-xs-12 col-md-4">
            <ul class="task-list">
                @foreach($pbis as $pbi)
                    @if ($pbi->days === 2)
                        <a href="/historias/{{ $pbi->id }}" > {{ $pbi->name }}</a>
                    @endif
                @endforeach
                
            </ul>
        </div>
        
        <div class="col-xs-12 col-md-4">
            <ul class="task-list">
                    <div>
                        <div class="show-on-hover pull-right">
                        </div>
                    </div>
                    <div>

                    </div>
                </li>
            </ul>
        </div>
    </div>


@endsection