@extends('layouts.app')

@section('content')
 {{--
<h1>Project Task List for:  "{{ $p_name->project_name }}" </h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Task Title</th>
            <th>Project Name</th>
            <th>Priority</th>
            <th>Status</th>
            <th>Actions</th>
        </tr>
        </thead>

             @if ( !$task_list->isEmpty() ) 
        <tbody>
            @foreach ( $task_list as $task)
            <tr>
                <td>{{ $task->task_title }} </td>
        </tbody>
       
        
    </table>

 ---}}
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">
                <span class="glyphicon glyphicon-comment"></span> 
                 <b> Historias de usuario </b>
              
                 <a href="{{ route('pbis.create', ['idsprint' => $idsprint->id]) }}" 
                        class="pull-right btn btn-primary btn-sm">
                        <span 
                        class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Crear nuevo 
                      </a> 
                    
             </h3>
            
        </div> 

        <div class="panel-body">
            
            <ul class="list-group">
                @foreach($pbis as $pbi)
                    <li class="list-group-item"> 
                        <i class="fa fa-play" aria-hidden="true"></i>
                        <a href="/tareas/{{ $pbi->id }}" > {{ $pbi->titulo }}</a>             
                    </li>
                @endforeach
            </ul>
        </div>

    </div>

    

@endsection