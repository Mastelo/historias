<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sprint;
use App\Project;
use Session;
use Illuminate\Support\Facades\Auth;

class SprintsControlador extends Controller
{

    

    public function show(Sprint $sprint)
    {
        $idsprint = Sprint::find($sprint->id);
        //dd($idsprint->id);
        //$comments = $project->comments;
        $pbis =  \DB::table('sprints')
        ->join('pbis', 'sprints.id', '=', 'pbis.sprint_id')
        ->where('sprints.id', $idsprint->id)
        ->get();
         return view('sprints.show', compact ('pbis','idsprint'));
    }

    public function store(Request $request)
    {
        $wow = request()->idsprint;
        //$idsprint = Sprint::find($sprint->id);
        //dd($wow);
        return view('sprints.create', compact('wow'));
    }

    public function create(Request $request)
    {
         if(Auth::check()){
            $comment = Sprint::create([
                'nombre' => $request->input('name'),
                //'' => $request->input('description'),
                'project_id' => $request->input('idproject'),
            ]);


            if($comment){
                //return back()->with('success' , 'Sprint created successfully');
                return redirect()->route('projects.show', ['project'=> $comment->project_id])
                 ->with('success' , 'Sprint creado exitosamente');
            }

        }  
            return back()->withInput()->with('errors', 'Error al crear nuevo sprint');
    }

    public function edit($id)
     {

         $project =  \DB::table('sprints')
        ->join('projects', 'sprints.project_id', '=', 'projects.id')
        ->where('sprints.id', $id)
        ->get()->first();
         //dd($project);
         return view('sprints.edit', compact ('project','id'));

        
        //return view('sprints.edit', compact ('id','idsprint'));
     }
    
     public function update(Request $request)
     {

        $id = $request->input('sprint_id');
       /* $this->validate( $request, [     
            'name'       => 'required',
            //'project_id' => 'required|numeric',
        ]) ; */
        //dd($id);
       $projectUpdate = Sprint::where('id', $id)
                        ->update([
                            
                            'nombre'=> $request->input('name'),
                            //'description'=> $request->input('description')
                        ]);
                        
       if($projectUpdate){
            return redirect()->route('projects.show', ['project'=> $id])
           ->with('success' , 'Se guardaron los datos del sprint');
             
           
       }
       //redirect
       return back()->withInput();
     }

     public function destroy($id)
     {
         $sprint = Sprint::find($id);
         if($sprint->delete()){ 
            Session::flash('success', 'File Deleted') ;
            return redirect()->back(); 
         }         
         return back()->withInput()->with('error' , 'project could not be deleted');  

     }
}
