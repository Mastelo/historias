<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Pbi;
use App\Sprint;
use App\Prioridad;
use Illuminate\Support\Facades\Auth;

class PbisController extends Controller
{
    public function index()
    {
        /*
        if( Auth::check() ){


            $companies = Company::where('user_id', Auth::user()->id)->get();

             return view('companies.index', ['companies'=> $companies]);  
        }*/
        //return view('auth.login');
    }
    /**
      * Display the specified resource.
      *
      * @param  \App\Pbi  $project
      * @return \Illuminate\Http\Response
      */
      /* borrar?
    public function show(Pbi $pbi)
    {
        $idpbi = Pbi::find($pbi->id);
 
        //$comments = $project->comments;
        $pbis =  \DB::table('pbis')
        ->where('pbis.id', $idpbi->id)
        ->get();
        //dd($pbis);
        //revisar
        return view('tareas.show', [  'pbis'=>$pbis ]);
    }*/


    /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
      public function create(Request $request)
      {
          $idPbi = request()->idsprint;
          //
          //dd($idPbi);
             $pbi = Pbi::where('id', $idPbi)->get()->first();
             $prioris = Prioridad::all();
             //dd($priori);
          return view('pbis.create', compact('pbi','idPbi','prioris'));
      }
  
      /**
       * Store a newly created resource in storage.
       *
       * @param  \Illuminate\Http\Request  $request
       * @return \Illuminate\Http\Response
       */
      public function store(Request $request)
      {
          $idsprint = $request->input('idSprint');

            $pbis =  \DB::table('pbis')
            ->where('pbis.sprint_id', $idsprint)
            ->get();
            //$wow = $request->input('priorida');
            //dd($wow);
          if(Auth::check()){
              $project = Pbi::create([
                  'titulo' => $request->input('name'),
                  'descripcion' => $request->input('description'),
                  'sprint_id' => $request->input('idSprint'), 
                  'prioridad_id' => $request->input('priorida'),        
              ]);
  
  
              if($project){

                  //return redirect()->route('sprints.show', compact('pbis'/*,'idsprint'*/) )
                  return redirect()->route('sprints.show', ['project'=> $idsprint])
                  ->with('success' , 'project created successfully');
              }
  
          }
          
              return back()->withInput()->with('errors', 'Error creating new project');
  
      }
}
