<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Prioridades extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('prioridades')->insert([
            'nombre' => 'Alta',
        ]);
        DB::table('prioridades')->insert([
            'nombre' => 'Media',
        ]);
        DB::table('prioridades')->insert([
            'nombre' => 'Baja',
        ]);
        DB::table('users')->insert([
            'email' => 'mast@gmail.com',
            'password' => bcrypt('123456'),
            'name' => 'Ronald'
        ]);

        DB::table('users')->insert([
            'email' => 'maste@gmail.com',
            'password' => bcrypt('123456'),
            'name' => 'Rodrigo'
        ]);
    }
}
