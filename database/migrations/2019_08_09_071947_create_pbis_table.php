<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePbisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pbis', function (Blueprint $table) {
            $table->increments('id');

            $table->string('titulo');
            $table->longText('descripcion')->nullable();
            //$table->longText('criterios')->nullable();
            $table->integer('estimacion')->nullable();
            $table->string('prioridad');
            $table->integer('sprint_id')->unsigned();
            $table->foreign('sprint_id')->references('id')->on('sprints');


            $table->integer('prioridad_id')->unsigned();
            $table->foreign('prioridad_id')->references('id')->on('prioridades');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pbis');
    }
}
